import {put, call,select,takeLatest} from 'redux-saga/effects';
import {notification} from 'antd';
import AppActions, {AppTypes} from '../actions/orderaction';
import ApiService from '../services/apiservices';

const getAppStore = state =>state.app;

export function* viewOrder(){
    console.log('view order saga');
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.getApi,
        //link,
        app.token,
    )
    console.log(result);

    if(result.flag){
        yield put(AppActions.viewOrderSuccess(result.data));
    }
    else{
        yield put(AppActions.viewOrderFail(result.message));
    }
}

export function* placeOrder(orderInfo){
    console.log('place order saga');
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.postApi,
        //link,
        app.token,
        orderInfo
    )
    console.log(result);
    if(result.flag){
        yield put(AppActions.placeORderSuccess(result.message));
    }
    else{
        yield put(AppActions.placeOrderFail(result.message));
    }
}

export function* viewOrderFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}

export function* placeOrderSuccess({successMessage}){
    yield call(notification.success,{
        message: successMessage,
    });
}

export function* placeOrderFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}

export const saga = [
    takeLatest(AppTypes.VIEW_ORDER,viewOrder),
    takeLatest(AppTypes.PLACE_ORDER,placeOrder),
    takeLatest(AppTypes.VIEW_ORDER_FAIL,viewOrderFail),
    takeLatest(AppTypes.PLACE_ORDER_SUCCESS,placeOrderSuccess),
    takeLatest(AppTypes.PLACE_ORDER_FAIL,placeOrderFail),
]