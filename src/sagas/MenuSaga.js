import {put, call,takeLatest,select} from 'redux-saga/effects';
import {notification} from 'antd';
import AppActions, {AppTypes} from '../actions/cartaction';
import ApiService from '../services/apiservices';

const getAppStore = state => state.app;


export function* getMenu(){
    console.log('get menu saga');
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.getApi,
        `http://127.0.0.1:8000/api/food/menu`,
        app.token,
    );
    console.log(result);
    if(result.flag){
        yield put(AppActions.getMenuSuccess(result.data));
    }
    else{
        yield put(AppActions.getMenuFail(result.message));
    }
}


export function* createFood(foodInfo){
    console.log('create food saga is called');
    const app = yield select(getAppStore);
    console.log(foodInfo);
    const result = yield call(
        ApiService.postApi,
        `http://127.0.0.1:8000/api/food/createFood`,
        app.token,
        foodInfo.foodInfo,
    );

    if(result.flag){
        yield put(AppActions.foodAddSuccess(result.message));
    }
    else{
        yield put(AppActions.foodAddFail(result.message));
    }
}

export function* deleteFood(id){
    console.log('delete food saga is called');
    console.log(id);
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.deleteApi,
        `http://127.0.0.1:8000/api/food/deleteFood/${id.id}`,
        app.token,
    );

    if(result.flag){
        yield put(AppActions.foodDeleteSuccess(result.message));
    }
    else{
        yield put(AppActions.foodDeleteFail(result.message));
    }
}

export function* updateFood(id,data){
    console.log('update food saga is called');
    console.log(id);
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.putApi,
        `http://127.0.0.1:8000/api/food/updateFood/${id.id}`,
        app.token,
        data
    );

    if(result.flag){
        yield put(AppActions.foodUpdateSuccess(result.message));
    }
    else{
        yield put(AppActions.foodUpdateFail(result.message));
    }
}

export function* findFood(id){
    console.log('find food saga is called');
    console.log(id);
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.getApi,
        `http://127.0.0.1:8000/api/food/findFood/${id.id}`,
        app.token,
    )
    console.log(result.data);
    if(result.flag){
        yield put(AppActions.foodFindSuccess(result.data));
    }
    else{
        yield put(AppActions.foodFindFail(result.message));
    }
}

export function* getMenuFail({errorMessage}){
    yield call(notification.error, {
        message: errorMessage,
      });
}



export function* foodAddSuccess({successMessage}){
    yield put(AppActions.getMenu());
    yield call(notification.success,{
        message: successMessage,
    });
}

export function* foodAddFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}

export function* foodDeleteSuccess({successMessage}){
    yield put(AppActions.getMenu());
    yield call(notification.success,{
        message: successMessage,
    });
}

export function* foodDeleteFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}

export function* foodUpdateSuccess({successMessage}){
    yield put(AppActions.getMenu());
    yield call(notification.success,{
        message: successMessage,
    });
}

export function* foodUpdateFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}


export function* foodFindFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    });
}

export const saga = [
    takeLatest(AppTypes.GET_MENU,getMenu),
    takeLatest(AppTypes.CREATE_FOOD,createFood),
    takeLatest(AppTypes.UPDATE_FOOD,updateFood),
    takeLatest(AppTypes.DELETE_FOOD,deleteFood),
    takeLatest(AppTypes.FIND_FOOD,findFood),
    takeLatest(AppTypes.GET_MENU_FAIL,getMenuFail),
    takeLatest(AppTypes.FOOD_ADD_SUCCESS,foodAddSuccess),
    takeLatest(AppTypes.FOOD_ADD_FAIL,foodAddFail),
    takeLatest(AppTypes.FOOD_DELETE_SUCCESS,foodDeleteSuccess),
    takeLatest(AppTypes.FOOD_DELETE_FAIL,foodDeleteFail),
    takeLatest(AppTypes.FOOD_UPDATE_SUCCESS,foodUpdateSuccess),
    takeLatest(AppTypes.FOOD_UPDATE_FAIL,foodUpdateFail),
    takeLatest(AppTypes.FOOD_FIND_FAIL,foodFindFail),
]