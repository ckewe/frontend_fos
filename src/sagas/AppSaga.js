import {put, call,select,takeLatest} from 'redux-saga/effects';
import {notification} from 'antd';
import AppActions, {AppTypes} from '../actions/useraction';
import ApiService from '../services/apiservices';

const getAppStore = state => state.app;

export function* login(email){
    console.log('login saga is called');
    const app = yield select(getAppStore);
    console.log(app);
    console.log(email)
    const result = yield call(
        ApiService.postApi,
        `http://127.0.0.1:8000/api/login`,
        `1035`,
        email
    );
    console.log(result);
    const userInfo = {
        email: result.email,
        token: result.newToken,
        success: result.flag,
        class: result.sclass,
        name: result.name,
        studentid: result.studentid
    }

    console.log(userInfo);

    if(result.flag ===true){
        yield put(AppActions.appAuthenticateSuccess(userInfo,result.message));
    }
    else {
      yield put(AppActions.appAuthenticateFail(result.message));
    }
}

export function* registration(user){
    console.log('register saga is called');
    console.log(user);

    const result = yield call(
        ApiService.postApi,
        `http://127.0.0.1:8000/api/register`,
        `1035`,
        user.user,
    )

    if(result.flag===true){
        yield put(AppActions.appRegistrationSuccess(user.user,result.message));
    }
    else{
        yield put(AppActions.appRegistrationfail(result.message));
    }
}

export function* logout(){
    console.log('logout saga');
    let tmp = false;
    const app = yield select(getAppStore);
    const data = {};
    const result = yield call(
        ApiService.postApi,
        `http://127.0.0.1:8000/api/logout`,
        app.token,
        data
    )
    console.log(result.message);
    if(result.flag){
        yield put(AppActions.appLogoutSuccess(tmp,result.message));
    }

}


export function* getUserList(){
    console.log('student list saga');
    const app = yield select(getAppStore);

    const result = yield call(
        ApiService.getApi,
        `http://127.0.0.1:8000/api/user`,
        app.token
    )
    console.log(result);
    if(result.flag){
        yield put(AppActions.getUserListSuccess(result.data));
    }
    else{
        yield put(AppActions.getUserListFail(result.message));
    }
}

export function* deleteUser(id){
    console.log('delete user');
    console.log(id);
    const app = yield select(getAppStore);
    const result = yield call(
        ApiService.deleteApi,
        `http://127.0.0.1:8000/api/user/deleteAcc/${id.id}`,
        app.token
    )
    console.log(result);
    if(result.flag){
        yield put(AppActions.deleteUserSuccess(result.message));
    }
    else{
        yield put(AppActions.deleteUserFail(result.message));
    }
}

export function* appChangePassword(currentpassword){
    console.log('changepassword saga')
    const app = yield select(getAppStore);
    console.log(currentpassword);
    const result = yield call(
        ApiService.putApi,
        `http://127.0.0.1:8000/api/user/changePassword`,
        app.token,
        currentpassword
    )

    if(result.flag){
        yield put(AppActions.appChangePasswordSuccess(result.message));
    }
    else{
        yield put(AppActions.appChangePasswordFail(result.message));
    }

}



export function* appAuthenticateSuccess({ successMessage }) {
    yield call(notification.success, {
      message: successMessage,
      
    });
}

export function* appAuthenticateFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
       
    })
}

export function* appRegistrationSuccess({successMessage}){
    yield call(notification.success,{
        message:successMessage,
       
    })
}

export function* appRegistrationFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage, 
    })
}

export function* appLogoutSuccess({successMessage}){
    yield call(notification.success,{
        message:successMessage,   
    })
}

export function* getUserListFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    })
}

export function* deleteUserSuccess({successMessage}){
    yield put(AppActions.getUserList());
    yield call(notification.success,{
        message:successMessage,   
    })
}

export function* deleteUserFail({errorMessage}){
    yield call(notification.error,{
        message: errorMessage,
    })
}

export function* appChangePasswordSuccess({successMessage}){
    yield call(notification.success,{
        message:successMessage,   
    })
}

export function* appChangePasswordFail({errorMessage}){
    yield call(notification.error,{
        message:errorMessage,   
    })
}

export const saga = [
    takeLatest(AppTypes.LOGIN, login),
    takeLatest(AppTypes.REGISTRATION, registration),
    takeLatest(AppTypes.LOGOUT,logout),
    takeLatest(AppTypes.GET_USER_LIST,getUserList),
    takeLatest(AppTypes.DELETE_USER,deleteUser),
    takeLatest(AppTypes.APP_CHANGE_PASSWORD,appChangePassword),
    takeLatest(AppTypes.APP_REGISTRATION_SUCCESS,appRegistrationSuccess),
    takeLatest(AppTypes.APP_REGISTRATION_FAIL,appRegistrationFail),
    takeLatest(AppTypes.APP_AUTHENTICATE_SUCCESS,appAuthenticateSuccess),
    takeLatest(AppTypes.APP_AUTHENTICATE_FAIL,appAuthenticateFail),
    takeLatest(AppTypes.APP_LOGOUT_SUCCESS,appLogoutSuccess),
    takeLatest(AppTypes.GET_USER_LIST_FAIL,getUserListFail),
    takeLatest(AppTypes.DELETE_USER_SUCCESS,deleteUserSuccess),
    takeLatest(AppTypes.DELETE_USER_FAIL,deleteUserFail),
    takeLatest(AppTypes.APP_CHANGE_PASSWORD_SUCCESS,appChangePasswordSuccess),
    takeLatest(AppTypes.APP_CHANGE_PASSWORD_FAIL,appChangePasswordFail),
]
