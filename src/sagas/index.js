import {all} from 'redux-saga/effects';
import {saga as MenuSaga} from './MenuSaga';
import {saga as AppSaga} from './AppSaga';
import {saga as OrderSaga} from './OrderSaga';

export default function* root(){
    yield all([
        ...AppSaga,
        ...MenuSaga,
        ...OrderSaga,
    ])
}