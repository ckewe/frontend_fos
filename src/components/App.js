import React from "react";
import "../App.css";
import "antd/dist/antd.css";
import {
  ShoppingCartOutlined,
  UserOutlined,
  HomeFilled,
  FormOutlined,
  SolutionOutlined,
  SyncOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import {
  Button,
  Layout,
  Menu,
  Table,
  Typography,
  Tag,
  notification,
  Modal,
  Form,
  Input,
  Select,
  DatePicker,
  Popconfirm,
} from "antd";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AppActions from "../actions/cartaction";
import UserActions from "../actions/useraction";

const { Column } = Table;
const { Title } = Typography;

const { Header, Content } = Layout;
const { Option } = Select;
export const orderlist = [];

function addToCart(name, type, days, session, price) {
  orderlist.push({
    name: name,
    tags: type,
    day: days,
    session: session,
    price: price,
    quantity: 1,
  });

  notification.success({ message: "Item added" });
  console.log({ orderList: orderlist });
}

class App extends React.Component {
  state = {
    food: [],
    createFood: false,
    updateFood: false,
    foodName: "",
    foodPrice: "",
    foodSession: "",
    foodDays: "",
    foodType: "",
    startDate: "",
    dessert: "",
  };

  componentDidMount() {
    const { getMenu } = this.props;
    getMenu();
    // fetch("http://127.0.0.1:8000/api/food/menu")
    //   .then((res) => res.json())
    //   .then((data) => {
    //     this.setState({ food: data.message });
    //     console.log(this.state.food);
    //   })
    //   .catch(console.log);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.menu.foodList !== this.props.menu.foodList) {
      this.setState({ food: this.props.menu.foodList });
    }
    if (this.state.foodName !== this.props.menu.foodName) {
      this.setState({
        foodName: this.props.menu.foodName,
        foodPrice: this.props.menu.foodPrice,
        foodSession: this.props.menu.foodSession,
        foodDays: this.props.menu.foodDays,
        foodType: this.props.menu.foodType,
        startDate: this.props.menu.startDate,
        dessert: this.props.menu.dessert,
      });
    }
    //console.log(this.state.foodName);
  }

  getFoodByID(id) {
    const { findFood } = this.props;
    findFood(id);

    // let url = "http://127.0.0.1:8000/api/food/findFood/" + id;
    // console.log({ url: url });

    // fetch(url)
    //   .then((res) => res.json())
    //   .then((data) => {
    //     if (!data.flag) {
    //       notification.error({ message: data.message });
    //     }

    //     this.setState({
    //       foodName: data.message.name,
    //       foodPrice: data.message.price,
    //       foodSession: data.message.session,
    //       foodDays: data.message.days,
    //       foodType: data.message.type,
    //       startDate: data.message.start_from,
    //       dessert: data.message.dessert,
    //     });
    //   });
  }

  showCreateFood = () => {
    this.setState({
      createFood: true,
    });
  };

  showUpdateFood = (food) => {
    this.setState({
      updateFood: true,
    });

    this.getFoodByID(food.id);
  };

  deleteFood(id) {
    const { deleteFood } = this.props;
    deleteFood(id);

    // let url = "http://127.0.0.1:8000/api/food/deleteFood/" + id;
    // console.log({ id: id });
    // fetch(url, {
    //   method: "DELETE",
    // })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     if (data.flag) {
    //       notification.success({ message: data.message });
    //     } else {
    //       notification.error({ message: data.message });
    //     }
    //   });
  }

  handleCancel = () => {
    this.setState({ createFood: false, updateFood: false });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    console.log({ [name]: value });
  };

  addMenu = () => {
    console.log(this.state.foodName);
    console.log(this.state.foodType);
    console.log(this.state.foodDays);
    console.log(this.state.foodSession);
    console.log(this.state.foodPrice);
    const { createFood } = this.props;
    let food = {
      name: this.state.foodName,
      price: this.state.foodPrice,
      session: this.state.foodSession,
      days: this.state.foodDays,
      type: this.state.foodType,
      dessert: this.state.dessert,
      start_from: this.state.startDate,
    };
    createFood(food);

    // fetch("http://127.0.0.1:8000/api/food/createFood", {
    //   method: "POST",
    //   headers: {
    //     accept: "application/json",
    //     "content-type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     name: this.state.foodName,
    //     price: this.state.foodPrice,
    //     session: this.state.foodSession,
    //     days: this.state.foodDays,
    //     type: this.state.foodType,
    //     dessert: this.state.dessert,
    //     start_from: this.state.startDate,
    //   }),
    // })
    //   .then(function (response) {
    //     return response.json();
    //   })
    //   .then(function (data) {
    //     if (data.flag) {
    //       notification.success({ message: data.message });
    //     } else {
    //       notification.warn({ message: data.message });
    //     }
    //     console.log(data);
    //   });

    // this.setState({ createFood: false });
    // setTimeout(() => {
    //   this.setState({ loading: false, createFood: false });
    //   notification.success({ message: "Menu Added Successfully" });
    // }, 1000);
  };

  MenuType = (value) => {
    if (value === "VEGE" || value === "NON-VEGE") {
      this.setState({ foodType: value });
      console.log({ foodType: value });
    } else if (
      value === "MON" ||
      value === "TUE" ||
      value === "WED" ||
      value === "THU" ||
      value === "SUN"
    ) {
      this.setState({ foodDays: value });
      console.log({ foodDays: value });
    } else if (value === "1" || value === "2") {
      this.setState({ foodSession: value });
      console.log({ foodSession: value });
    }
  };

  SetDessertType = (value) => {
    this.setState({ dessert: value });
    console.log({ dessert: value });
  };

  SetStartDate = (date, dateString) => {
    this.setState({ startDate: dateString });
    console.log({ startDate: dateString });
  };

  postError = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  logout = (e) => {
    this.props.logout();
  };

  render() {
    const {
      createFood,
      updateFood,
      foodName,
      foodPrice,
      foodSession,
      foodDays,
      foodType,
      startDate,
      dessert,
    } = this.state;

    const myOwnFoodName = "";
    return (
      <Layout>
        <Header>
          <Menu
            theme="dark"
            defaultOpenKeys={["1"]}
            breakpoint="lg"
            selectedKeys={["1"]}
            mode="horizontal"
            onBreakpoint={(broken) => {
              console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
              console.log(collapsed, type);
            }}
          >
            <Title
              level={5}
              style={{
                float: "left",
                color: "white",
                padding: " 23px 30px 0px 0px",
                fontSize: "14px",
              }}
            >
              Food Ordering System
            </Title>
            <Menu.Item key="1" icon={<HomeFilled />}>
              Home
            </Menu.Item>
            <Menu.Item key="2" icon={<ShoppingCartOutlined />}>
              <Link style={{ textDecoration: "none" }} to="/checkout">
                Order List
              </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<SolutionOutlined />}>
              <Link style={{ textDecoration: "none" }} to="/studentlist">
                User List
              </Link>
            </Menu.Item>
            <Menu.Item key="4" icon={<FormOutlined />}>
              <Link style={{ textDecoration: "none" }} to="/report">
                Report
              </Link>
            </Menu.Item>
            <Menu.Item key="5" className="enter" onClick={this.logout}>
              <Link style={{ textDecoration: "none" }} to="/">
                Logout
              </Link>
            </Menu.Item>
            <Menu.Item key="6" icon={<UserOutlined />} className="enter">
              <Link style={{ textDecoration: "none" }} to="/profile">
                Profile
              </Link>
            </Menu.Item>
          </Menu>
        </Header>

        <Content style={{ padding: "0 20px", margin: "16px 0" }}>
          <div className="site-layout-content">
            <Button onClick={this.showCreateFood}>Create Food</Button>
            <p></p>
            <Table dataSource={this.state.food} pagination={{}}>
              <Column title="Name" dataIndex="name" />
              <Column
                title="Type"
                dataIndex="type"
                sorter={(a, b) => b.type.length - a.type.length}
                render={(type) => {
                  if (type === "VEGE") {
                    return <Tag color={"green"}>{type.toUpperCase()}</Tag>;
                  } else if (type === "NON-VEGE") {
                    return <Tag color={"geekblue"}>{type.toUpperCase()}</Tag>;
                  }
                }}
              />
              <Column
                title="Days"
                dataIndex="days"
                render={(days) => {
                  if (days === "MON") {
                    return <Tag color={"green"}>{days.toUpperCase()}</Tag>;
                  } else if (days === "TUE") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  } else if (days === "WED") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  } else if (days === "THU") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  } else if (days === "FRI") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  } else if (days === "SAT") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  } else if (days === "SUN") {
                    return <Tag color={"geekblue"}>{days}</Tag>;
                  }
                }}
              />
              <Column
                title="Session"
                dataIndex="session"
                sorter={(a, b) => b.session - a.session}
                render={(session) => {
                  if (session === 1) {
                    return <Tag color={"#FFC500"}>MORNING</Tag>;
                  } else if (session === 2) {
                    return <Tag color={"#FF3F36"}>AFTERNOON</Tag>;
                  }
                }}
              />
              <Column
                title="Price (RM)"
                dataIndex="price"
                sorter={(a, b) => a.price - b.price}
              />
              <Column
                title="Action"
                render={(food) => [
                  <Button
                    type="primary"
                    size="middle"
                    icon={<ShoppingCartOutlined />}
                    onClick={(event) => {
                      event.preventDefault();
                      addToCart(
                        food.name,
                        food.type,
                        food.days,
                        food.session,
                        food.price
                      );
                    }}
                  >
                    Add
                  </Button>,

                  <Button
                    style={{ marginLeft: "10px" }}
                    onClick={(event) => {
                      event.preventDefault();
                      this.showUpdateFood(food);
                    }}
                    icon={<SyncOutlined />}
                  >
                    Update
                  </Button>,
                  <Popconfirm
                    title="Sure to delete?"
                    onConfirm={(event) => {
                      event.preventDefault();
                      this.deleteFood(food.id);
                    }}
                  >
                    <Button
                      danger
                      style={{ marginLeft: "10px" }}
                      icon={<DeleteOutlined />}
                    >
                      Delete
                    </Button>
                  </Popconfirm>,
                ]}
              />
            </Table>
          </div>
        </Content>

        {/* //createfood */}
        <Modal
          visible={createFood}
          onCancel={this.handleCancel}
          title="New Food"
          footer={null}
        >
          <Form onFinish={this.addMenu} onFinishFailed={this.postError}>
            <Form.Item
              label="Name"
              name="name"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Input
                name="foodName"
                value={this.state.foodName}
                onChange={this.handleChange}
              />
            </Form.Item>
            <Form.Item
              label="Type"
              name="type"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select
                initialValue={this.state.foodType}
                onChange={this.MenuType}
              >
                <Option value="VEGE">Vegetarian</Option>
                <Option value="NON-VEGE">Non-Vegetarian</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Days"
              name="days"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select
                initialValue={this.state.foodType}
                onChange={this.MenuType}
              >
                <Option value="SUN">Sunday</Option>
                <Option value="MON">Monday</Option>
                <Option value="TUE">Tuesday</Option>
                <Option value="WED">Wednesday</Option>
                <Option value="THU">Thursday</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Session"
              name="session"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select
                initialValue={this.state.foodType}
                onChange={this.MenuType}
              >
                <Option value="1">Morning</Option>
                <Option value="2">Afternoon</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Price (ex: 1.00)"
              name="price"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
                {
                  pattern: new RegExp("^[0-9]+(.[0-9]{1,2})?$"),
                  message: "The format is invalid",
                },
              ]}
            >
              <Input
                name="foodPrice"
                value={this.state.foodPrice}
                onChange={this.handleChange}
              />
            </Form.Item>

            <Form.Item
              label="Dessert"
              name="dessert"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select onChange={this.SetDessertType}>
                <Option value="1">Side Dish</Option>
                <Option value="0">Main Dish</Option>
              </Select>
            </Form.Item>

            <Form.Item
              label="Start Date"
              name="date"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <DatePicker onChange={this.SetStartDate} />
            </Form.Item>

            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button
              type="default"
              style={{ marginLeft: "5px" }}
              onClick={this.handleCancel}
            >
              Cancel
            </Button>
          </Form>
        </Modal>

        {/* updateFood */}
        <Modal
          visible={updateFood}
          onCancel={this.handleCancel}
          title="Update Food Details"
          footer={null}
        >
          <Form
            onFinish={this.addMenu}
            onFinishFailed={this.postError}
            initialValues={{
              name: this.state.foodName,
              type: this.state.foodType,
              days: this.state.foodDays,
              session: this.state.foodSession,
              date: this.state.startDate,
              dessert: this.state.dessert,
            }}
          >
            <Form.Item
              label="Name"
              name="name"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Input type="text" onChange={this.handleChange} />
            </Form.Item>
            <Form.Item
              label="Type"
              name="type"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select onChange={this.MenuType}>
                <Option value="VEGE">Vegetarian</Option>
                <Option value="NON-VEGE">Non-Vegetarian</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Days"
              name="days"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select onChange={this.MenuType}>
                <Option value="SUN">Sunday</Option>
                <Option value="MON">Monday</Option>
                <Option value="TUE">Tuesday</Option>
                <Option value="WED">Wednesday</Option>
                <Option value="THU">Thursday</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Session"
              name="session"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select onChange={this.MenuType}>
                <Option value="1">Morning</Option>
                <Option value="2">Afternoon</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Price (ex: 1.00)"
              name="price"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
                {
                  pattern: new RegExp("^[0-9]+(.[0-9]{1,2})?$"),
                  message: "The format is invalid",
                },
              ]}
            >
              <Input onChange={this.handleChange} />
            </Form.Item>

            <Form.Item
              label="Dessert"
              name="dessert"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <Select onChange={this.SetDessertType}>
                <Option value="1">Side Dish</Option>
                <Option value="0">Main Dish</Option>
              </Select>
            </Form.Item>

            <Form.Item
              label="Start Date"
              name="date"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "This field is Required!",
                },
              ]}
            >
              <DatePicker onChange={this.SetStartDate} />
            </Form.Item>

            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button
              type="default"
              style={{ marginLeft: "5px" }}
              onClick={this.handleCancel}
            >
              Cancel
            </Button>
          </Form>
        </Modal>
      </Layout>
    );
  }
}
const mapStatetoProps = (state) => ({
  menu: state.cart,
  app: state.app,
});

App.propTypes = {
  getMenu: PropTypes.func,
  findFood: PropTypes.func,
  deleteFood: PropTypes.func,
  updateFood: PropTypes.func,
  createFood: PropTypes.func,
  logout: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
  getMenu: () => dispatch(AppActions.getMenu()),
  findFood: (id) => dispatch(AppActions.findFood(id)),
  deleteFood: (id) => dispatch(AppActions.deleteFood(id)),
  updateFood: (id) => dispatch(AppActions.updateFood(id)),
  createFood: (foodInfo) => dispatch(AppActions.createFood(foodInfo)),
  logout: () => dispatch(UserActions.logout()),
});

export default connect(mapStatetoProps, mapDispatchToProps)(App);
