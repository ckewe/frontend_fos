import React from 'react';
import 'antd/dist/antd.css';
import '../App.css';
import { Form, Input, Button, Card, notification,  Modal } from 'antd';
import {UserOutlined, LockOutlined, CheckCircleTwoTone,StopTwoTone} from '@ant-design/icons';
import {Helmet} from 'react-helmet';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import AppActions from '../actions/useraction';
import {Formik} from 'formik';
import * as Yup from 'yup';
import Error from './Error';


const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

export const userInfo= {
  name: '',
  email: '',
  class: '',
  studentid: '',
  role: '',
}

class Login extends React.Component {
state = {
        email: '',
        password: '',

        visible: false,
        redirect: false,
    };
    
    componentDidMount() {
    }

    
    componentDidUpdate(prevProps){

      if(this.props.logged.isLogged === true){
        this.setState({redirect: true});
      }

      if(prevProps.logged.registerStatus !== this.props.logged.registerStatus){
        setTimeout(() => {
          this.setState({visible:false});
          }, 1000);
      }
    }


    showModal = () => {
                this.setState({
                  visible: true,
                });
              };
        
    handleCancel = () => {
    this.setState({ visible: false });
    };
    
    handleChange = (e) => {    
        const { name, value } = e.target;    
        this.setState({ [name]: value });   
        //console.log({ [name]: value });
    }   
    
    validation = (e) =>{
        e.preventDefault();
        let {email,password} = this.state;
        let {login} = this.props;
        if(email&&password){ 
          login(email,password);
        }
        console.log(this.props.logged);
        
        
    }                                

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to='/home' />
        }
      }


    render() {
        const { visible } = this.state;
        return (
            <div>
                <Helmet>
                    <style>{'body { background-color: #efefef; }'}</style>
                </Helmet>
                <div className="page-title">
                    <h1>Food Ordering System</h1>
                    <p>By Author/Developer Whatevereuaeblkalka</p>
                </div>
                <div className="login-card">
                <Card title="Login" hoverable={true} style={{ width: 400, float:"right"}}>
                <Form
                {...layout}
                >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                    {
                        required: true,
                        message: 'Required Field!',
                    },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />} name="email" value={this.state.email} onChange={this.handleChange}/>
                </Form.Item>
            
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Required Field!',
                    },
                    ]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} name="password" value={this.state.password} onChange={this.handleChange} />
                </Form.Item>
                    {this.renderRedirect()}
                    <Button type="primary" htmlType="submit" onClick={this.validation} style={{ marginLeft:"110px"}}>
                    Login
                    </Button>
                    <Button type="default" style={{ marginLeft:"5px"}} onClick={this.showModal}>
                        Create New Account
                    </Button>
                </Form>
                </Card>
                
                </div>


                <Modal
          visible={visible}
          onCancel={this.handleCancel}
          title="User Registration"
          footer={null}
        >
          <Formik initialValues={{
          name: '',
          registeremail: '',
          registerpassword: '',
          confirmpassword: '',
          studentid: '',
          studentclass:'',
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string()
            .min(2, "Must have a character")
            .max(255, "Must be shorter than 255")
            .required("Please input your name!"),

            registeremail: Yup.string()
            .email("Must be a valid email address")
            .max(255, "Invalid email address")
            .required("Please input your email!"),

            studentid: Yup.string()
            .min(1,"Must have a value")
            .max(8,"Student ID cannot be longer than 7 Digits")
            .required("Please enter your Student ID"),

            studentclass:Yup.string()
            .min(1,"Must have a character")
            .max(3,"Must be shorter than 4")
            .required("Please enter your Class"),

            registerpassword:Yup.string()
            .min(8,"Minimum 8 characters")
            .matches(
              /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
              "Password must contain at least 8 characters, one uppercase, one number and one special case character"
            )
            .required("Please enter your password"),
            
            confirmpassword: Yup.string()
            .oneOf([Yup.ref('registerpassword'), null], "Passwords don't match.")
            .required("Please confirm your password"),
          })}
          onSubmit={(values,{resetForm}) =>{
            let {registration} = this.props;
            console.log(values.name);
            const user = {
              name: values.name,
              role: 'Teacher',
              email: values.registeremail,
              password: values.registerpassword,
              confirm_password: values.confirmpassword,
              student_id: values.studentid,
              class: values.studentclass,
            }
            registration(user);
            console.log(this.props.logged.registerStatus);

            setTimeout(() => {
              resetForm();
              }, 3000);            
          }}

          >
            {({values,errors,touched,handleChange,handleBlur,handleSubmit})=>(
          <Form
            {...layout}  
          >
            {JSON.stringify(values)}
            <Form.Item label="Name" name="name">
              <Input 
              name="name" 
              style={touched.name && errors.name ? {borderColor: "red"} : null} 
              value={values.name} 
              onBlur={handleBlur} 
              onChange={handleChange}
              />
              <Error touched={touched.name} message={errors.name} />
            </Form.Item>
           <Form.Item label="E-Mail" name="email">
              <Input 
              name="registeremail" 
              value={values.registeremail} 
              style={touched.registeremail && errors.registeremail ? {borderColor: "red"} : null} 
              onBlur={handleBlur} 
              onChange={handleChange}/>
              <Error touched={touched.registeremail} message={errors.registeremail} />
            </Form.Item>
            <Form.Item label="Student ID" name="studentid">
              <Input 
              name="studentid" 
              value={values.studentid}
              style={touched.studentid && errors.studentid ? {borderColor: "red"} : null} 
              onBlur={handleBlur} 
              onChange={handleChange}/>
              <Error touched={touched.studentid} message={errors.studentid} />
            </Form.Item>
            <Form.Item label="Class" name="studentclass">
              <Input 
              name="studentclass" 
              value={values.studentclass}
              style={touched.studentclass && errors.studentclass ? {borderColor: "red"} : null} 
              onBlur={handleBlur} 
              onChange={handleChange}/>
              <Error touched={touched.studentclass} message={errors.studentclass} />
            </Form.Item>
            <Form.Item label="Password" name="registerpassword">
              <Input.Password 
              name="registerpassword" 
              value={values.registerpassword}
              style={touched.registerpassword && errors.registerpassword ? {borderColor: "red"} : null} 
              onBlur={handleBlur} 
              onChange={handleChange}/>
              <Error touched={touched.registerpassword} message={errors.registerpassword} />
            </Form.Item>
            <Form.Item label="Confirm Password" name="confirm">
              <Input.Password
              name="confirmpassword"
              value={values.confirmpassword}
              style={touched.confirmpassword && errors.confirmpassword ? {borderColor: "red"} : null} 
              onBlur={handleBlur} 
              onChange={handleChange}
              />
              <Error touched={touched.confirmpassword} message={errors.confirmpassword} />
            </Form.Item>
                <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Submit
                </Button>
                <Button type="default" style={{ marginLeft:"5px"}} onClick={this.handleCancel}>
                    Cancel
                </Button>
        
            </Form>
            )}
          
          </Formik>
          
        </Modal>
                
                
            </div>  
        );
        
        
    }


}

const mapStatetoProps = state =>({
  logged: state.app,
});



Login.propTypes = {
  login: PropTypes.func,
  registration: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
  login: (email,password) =>
    dispatch(AppActions.login(email,password)),
  registration: (user) =>
    dispatch(AppActions.registration(user)),
})


export default connect(mapStatetoProps,mapDispatchToProps)(Login);








