import React from 'react';
import '../App.css';
import 'antd/dist/antd.css';
import {
    ShoppingCartOutlined,
    UserOutlined,
    HomeFilled,
    FormOutlined,
    SolutionOutlined
   } from '@ant-design/icons';
import { Button, Layout, Menu, Typography, Form, Input, Modal, Descriptions } from 'antd';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import AppActions from '../actions/useraction';
import {Formik} from 'formik';
import * as Yup from 'yup';
import Error from './Error'; 

const { Title } = Typography;
const {Header, Content} = Layout;


const formlayout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 16,
  },
}

class Profile extends React.Component {
    state= {
        visible: false,
        newpassword: "",
        confirmpassword: '',
        currentpassword:'',
    };

    componentDidMount(){
      console.log(this.props.app);
    }

    showModal = () => {
      this.setState({
        visible: true,
      });
    };

    logout = (e) =>{
      this.props.logout();
    }

    handleChange = (e) => {    
      const { name, value } = e.target;    
      this.setState({ [name]: value });   
    };

    handleCancel = e => {
      this.setState({
        visible: false,
      });
    };

    render() {
      const {app} = this.props;
        return (
          
          <Layout>    
            <Header>
              <Menu
              theme="dark"
              breakpoint="lg"
              selectedKeys={['6']}
              mode="horizontal"
              onBreakpoint={broken => {
                console.log(broken);
              }}
              onCollapse={(collapsed, type) => {
                console.log(collapsed, type);
              }}
              >
              <Title level={5} style={{float:"left", color:"white", padding: " 23px 30px 0px 0px", fontSize:"14px"}}>Food Ordering System</Title>
              <Menu.Item key="1" icon={<HomeFilled/>}>
                <Link style={{textDecoration:"none"}} to="/home">
                    Home
                </Link>
              </Menu.Item>
              
              <Menu.Item key="2"icon={<ShoppingCartOutlined/>}>
                <Link style={{textDecoration:"none"}} to="/checkout">
                  Order List
                </Link>
              </Menu.Item>          
              <Menu.Item key="3" icon={<SolutionOutlined/>}>
                <Link style={{textDecoration:"none"}} to="/studentlist">
                  User List
                </Link>
              </Menu.Item>
              <Menu.Item key="4" icon={<FormOutlined/>}>
                <Link style={{textDecoration:"none"}} to="/report">
                  Report
                </Link>
              </Menu.Item>
              <Menu.Item key="5" className="enter" onClick={this.logout}>
                <Link style={{textDecoration:"none"}} to="/">
                Logout
                </Link>
              </Menu.Item>
              <Menu.Item key="6" icon={<UserOutlined/>} className="enter">
                Profile
              </Menu.Item>       
    
              </Menu>
            </Header>
        <Content style={{padding: '0 20px', margin:'16px 0'}}>
        <div className="site-layout-content">
        <Descriptions title="User Profile" layout="vertical">
            <Descriptions.Item label="Name">{app.name}</Descriptions.Item>
            <Descriptions.Item label="Email">{app.email}</Descriptions.Item>
            <Descriptions.Item label="Student Id">{app.studentid}</Descriptions.Item>
            <Descriptions.Item label="Class">{app.class}</Descriptions.Item>
        </Descriptions>

            <Button onClick={this.showModal}>Change Password</Button>
            <Modal title="Change Password" visible={this.state.visible} onCancel={this.handleCancel} footer={null}>
              <Formik initialValues={{
                currentpassword:'',
                newpassword: '',
                confirmpassword: '',
              }}
              validationSchema={Yup.object().shape({
                currentpassword:Yup.string()
                .min(8,"Minumum 8 characters")
                .required("Please enter your current password"),

                newpassword:Yup.string()
                .min(8,"Minimum 8 characters")
                .matches(
                  /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
                  "New password must contain at least 8 characters, one uppercase, one number and one special case character"
                )
                .required("Please enter your new password"),

                confirmpassword:Yup.string()
                .oneOf([Yup.ref('newpassword'), null], "Passwords don't match.")
                .required("Please confirm your new password"),
                
              })}
              onSubmit={(values,{resetForm})=>{
                const {changePassword} = this.props;
                changePassword(values.currentpassword,values.newpassword,values.confirmpassword);
                  
                setTimeout(() => {
                  this.setState({
                    visible: false,
                  });
                  resetForm();
                  }, 1000);        
              }}
              >
              {({values,errors,touched,handleChange,handleBlur,handleSubmit})=>(
                <Form {...formlayout}>
                <Form.Item label="Enter your current password">
                  <Input.Password 
                  name="currentpassword" 
                  style={touched.currentpassword && errors.currentpassword ? {borderColor: "red"} : null} 
                  value={values.currentpassword}
                  onBlur={handleBlur} 
                  onChange={handleChange}/>
                  <Error touched={touched.currentpassword} message={errors.currentpassword} />
                </Form.Item>
            <Form.Item label="New Password" name="newpassword">
                <Input.Password 
                name="newpassword" 
                style={touched.newpassword && errors.newpassword ? {borderColor: "red"} : null} 
                value={values.newpassword}
                onBlur={handleBlur} 
                onChange={handleChange}/>
                <Error touched={touched.newpassword} message={errors.newpassword} />
            </Form.Item>
            <Form.Item label="Confirm Password" name="confirmpassword">
                <Input.Password 
                name="confirmpassword" 
                style={touched.confirmpassword && errors.confirmpassword ? {borderColor: "red"} : null} 
                value={values.confirmpassword}
                onBlur={handleBlur} 
                onChange={handleChange}/>
                <Error touched={touched.confirmpassword} message={errors.confirmpassword} />
            </Form.Item>
            <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Change Password
                </Button>
                <Button type="default" style={{ marginLeft:"5px"}} onClick={this.handleCancel}>
                    Cancel
                </Button>
              </Form>
              )}
              </Formik>

              
            </Modal>
        </div>
        </Content>

         </Layout>
    
        );
      } 

}

const mapStatetoProps = state =>({
  app: state.app,
})

Profile.propTypes = {
  changePassword: PropTypes.func,
  logout: PropTypes.func,
}

const mapDispatchToProps = dispatch =>({
  changePassword: (old_password,new_password,confirm_password) =>
    dispatch(AppActions.appChangePassword(old_password,new_password,confirm_password)),
  logout: () =>
    dispatch(AppActions.logout()),
})

export default connect(mapStatetoProps,mapDispatchToProps)(Profile);