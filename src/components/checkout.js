//TODO: calculate the total price function (using map(?))

import React from "react";
import "antd/dist/antd.css";
import "../App.css";
import {
  ShoppingCartOutlined,
  UserOutlined,
  HomeFilled,
  FormOutlined,
  SolutionOutlined,
} from "@ant-design/icons";
import { orderlist } from "./App";
import { Helmet } from "react-helmet";
import {
  Layout,
  Menu,
  Typography,
  Table,
  Tag,
  Button,
  InputNumber,
} from "antd";
import { Link } from "react-router-dom";

const { Column } = Table;
const { Title } = Typography;
const { Header, Content } = Layout;

orderlist.map(calculateTotal);
var vall = 0;

function calculateTotal(item) {
  var tmp = item.quantity * item.price;
  return console.log(tmp);
}
function removeItem(rowNo) {
  orderlist.splice(rowNo, 1);
  console.log(orderlist);
}

function updateV(value) {
  vall = value;
}

function updateQuantity(rowNo, value) {
  orderlist[rowNo].quantity = value;
  console.log(orderlist);
}

class Checkout extends React.Component {



  render() {
    return (
      <div>
        <Helmet>
          <style>{"body { background-color: #efefef; }"}</style>
        </Helmet>
        <Layout>
          <Header>
            <Menu
              theme="dark"
              breakpoint="lg"
              selectedKeys={["2"]}
              mode="horizontal"
              onBreakpoint={(broken) => {
                console.log(broken);
              }}
              onCollapse={(collapsed, type) => {
                console.log(collapsed, type);
              }}
            >
              <Title
                level={5}
                style={{
                  float: "left",
                  color: "white",
                  padding: " 23px 30px 0px 0px",
                  fontSize: "14px",
                }}
              >
                Food Ordering System
              </Title>
              <Menu.Item key="1" icon={<HomeFilled />}>
                <Link style={{ textDecoration: "none" }} to="/home">
                  Home
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<ShoppingCartOutlined />}>
                Order List
              </Menu.Item>
              <Menu.Item key="3" icon={<SolutionOutlined />}>
                <Link style={{ textDecoration: "none" }} to="/studentlist">
                  User List
                </Link>
              </Menu.Item>
              <Menu.Item key="4" icon={<FormOutlined />}>
                <Link style={{ textDecoration: "none" }} to="/report">
                  Report
                </Link>
              </Menu.Item>
              <Menu.Item key="5" className="enter">
                <Link style={{ textDecoration: "none" }} to="/">
                  Logout
                </Link>
              </Menu.Item>
              <Menu.Item key="6" icon={<UserOutlined />} className="enter">
                <Link style={{ textDecoration: "none" }} to="/profile">
                  Profile
                </Link>
              </Menu.Item>
            </Menu>
          </Header>
          <Content style={{ padding: "0 20px", margin: "16px 0" }}>
            <div className="site-layout-content">
              <h1>Here's What You Ordered</h1>
              <Table dataSource={orderlist}>
                <Column title="Name" dataIndex="name" />
                <Column
                  title="Type"
                  dataIndex="tags"
                  render={(tags) => {
                    if (tags === "VEGE") {
                      return <Tag color={"green"}>{tags}</Tag>;
                    } else if (tags === "NON-VEGE") {
                      return <Tag color={"geekblue"}>{tags}</Tag>;
                    }
                  }}
                />
                <Column
                  title="Session"
                  dataIndex="session"
                  render={(session) => {
                    if (session === 1) {
                      return <Tag color={"#FFC500"}>MORNING</Tag>;
                    } else if (session === 2) {
                      return <Tag color={"#FF3F36"}>AFTERNOON</Tag>;
                    }
                  }}
                />
                <Column
                  title="Days"
                  dataIndex="day"
                  render={(day) => {
                    if (day === "MON") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "TUE") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "WED") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "THU") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "FRI") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "SAT") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    } else if (day === "SUN") {
                      return <Tag color={"geekblue"}>{day}</Tag>;
                    }
                  }}
                />
                <Column
                  title="Quantity"
                  onCell={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        updateQuantity(rowIndex, vall);
                      },
                    };
                  }}
                  dataIndex="quantity"
                  render={() => (
                    <InputNumber onChange={updateV} defaultValue={0} min={0} />
                  )}
                />
                <Column title="Price per Item (RM)" dataIndex="price" />
                <Column
                  onCell={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        removeItem(rowIndex);
                      },
                    };
                  }}
                  render={() => (
                    <Button type="primary" danger size="middle">
                      Remove
                    </Button>
                  )}
                />
              </Table>
              <h4>Total Price:</h4>
              <Button type="primary" size="large" block>
                Checkout
              </Button>
            </div>
          </Content>
        </Layout>
      </div>
    );
  }
}

export default Checkout;
