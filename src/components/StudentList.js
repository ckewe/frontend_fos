import React from 'react';
import 'antd/dist/antd.css';
import '../App.css';
import Highlighter from 'react-highlight-words';
import { SearchOutlined,
        ShoppingCartOutlined,
        UserOutlined,
        HomeFilled,
        FormOutlined,
        SolutionOutlined ,
        DeleteOutlined
      } from '@ant-design/icons';
import { Layout, Menu, Typography, Table, Input, Button, Space, Popconfirm } from 'antd';
import {Link} from 'react-router-dom';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import AppActions from '../actions/useraction';

const {Header,Content} = Layout;
const {Title} = Typography;

class StudentList extends React.Component {
  state = {
    searchText: '',
    searchedColumn: '',
    student: []
  };

  componentDidMount() {
    const {getUserList} = this.props;
    getUserList();
    // fetch('http://127.0.0.1:8000/api/user')
    // .then(res => res.json())
    // .then((data) => {
    //   this.setState({ student: data })
    // })
    // .catch(console.log)
  }

  componentDidUpdate(prevProps){
    if(prevProps.app.userList !== this.props.app.userList){
      this.setState({student: this.props.app.userList});
    }
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  delete(userId){
    console.log(userId);
    this.props.deleteUser(userId);
  }

  logout = (e) =>{
    this.props.logout();
  }

  render() {
    console.log(this.state.student);
    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '30%',
        ...this.getColumnSearchProps('name'),
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: '20%',
        ...this.getColumnSearchProps('email'),
      },
      {
        title: 'Class',
        dataIndex: 'class',
        key: 'class',
        ...this.getColumnSearchProps('class'),
      },
      {
        title: 'Student ID',
        dataIndex: 'student_id',
        key: 'studentid',
        ...this.getColumnSearchProps('student_id'),
      },
      {
        title:'Action',
        render: (student)=> 
        <Popconfirm
        title="Sure to delete?"
        onConfirm={(event) => {
          event.preventDefault();
          this.delete(student.id);
        }}
      >
      <Button danger icon={<DeleteOutlined />}>
        Delete
      </Button>
      </Popconfirm>
      }
    ];
    return(
      <div>
          <Helmet>
            <style>{'body { background-color: #efefef; }'}</style>
          </Helmet>
          <Layout>    
          <Header>
            <Menu
            theme="dark"
            breakpoint="lg"
            selectedKeys={['3']}
            mode="horizontal"
            onBreakpoint={broken => {
              console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
              console.log(collapsed, type);
            }}
            >
            <Title level={5} style={{float:"left", color:"white", padding: " 23px 30px 0px 0px", fontSize:"14px"}}>Food Ordering System</Title>
            <Menu.Item key="1" icon={<HomeFilled/>}>
              <Link style={{textDecoration:"none"}} to="/home">
                Home
              </Link>
            </Menu.Item>
            <Menu.Item key="2"icon={<ShoppingCartOutlined/>}>
            <Link style={{textDecoration:"none"}} to="/checkout">
                Order List
              </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<SolutionOutlined/>}>User List</Menu.Item>
            <Menu.Item key="4" icon={<FormOutlined/>}>
              <Link style={{textDecoration:"none"}} to="/report">
                Report
              </Link>
            </Menu.Item>
            <Menu.Item key="5" className="enter" onClick={this.logout}>
              <Link style={{textDecoration:"none"}} to="/">
                Logout
              </Link>
            </Menu.Item>
            <Menu.Item key="6" icon={<UserOutlined/>} className="enter">
              <Link style={{textDecoration:"none"}} to="/profile">
              Profile
              </Link>
            </Menu.Item>
            </Menu>
          </Header>
          <Content style={{padding: '0 20px', margin:'16px 0'}}>
          <div className="site-layout-content">

          <Table columns={columns} dataSource={this.state.student} />;
          </div>
          </Content>
          </Layout>
        </div>
    )
  }
}

const mapStatetoProps = state =>({
  app: state.app,
})

StudentList.propTypes = {
  getUserList: PropTypes.func,
  logout: PropTypes.func,
  deleteUser: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
  getUserList: () =>
    dispatch(AppActions.getUserList()),
  logout: ()=>
    dispatch(AppActions.logout()),
  deleteUser: (id) =>
    dispatch(AppActions.deleteUser(id)),
})

export default connect(mapStatetoProps,mapDispatchToProps)(StudentList);