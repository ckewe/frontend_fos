import React from 'react';
import 'antd/dist/antd.css';
import '../App.css';
import {
    ShoppingCartOutlined,
    UserOutlined,
    HomeFilled,
    FormOutlined,
    SolutionOutlined
   } from '@ant-design/icons';
import {Helmet} from 'react-helmet';
import {Layout, Menu, Typography,Row,Col} from 'antd';
import {Link} from 'react-router-dom';


const {Title} = Typography;
const {Header, Content} = Layout;


class Report extends React.Component {
render() {
    return(
        <div>
          <Helmet>
            <style>{'body { background-color: #efefef; }'}</style>
          </Helmet>
        <Layout>    
        <Header>
          <Menu
          theme="dark"
          breakpoint="lg"
          selectedKeys={['4']}
          mode="horizontal"
          onBreakpoint={broken => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
          >
          <Title level={5} style={{float:"left", color:"white", padding: " 23px 30px 0px 0px", fontSize:"14px"}}>Food Ordering System</Title>
          <Menu.Item key="1" icon={<HomeFilled/>}>
            <Link style={{textDecoration:"none"}} to="/home">
              Home
            </Link>
          </Menu.Item>
          <Menu.Item key="2"icon={<ShoppingCartOutlined/>}>
            <Link style={{textDecoration:"none"}} to="/checkout">
              Order List
            </Link>
          </Menu.Item>
          <Menu.Item key="3" icon={<SolutionOutlined/>}>
          <Link style={{textDecoration:"none"}} to="/studentlist">
              User List
            </Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<FormOutlined/>}>    
              Report
          </Menu.Item>
          <Menu.Item key="5" className="enter">
            <Link style={{textDecoration:"none"}} to="/">
            Logout
            </Link>
          </Menu.Item>
          <Menu.Item key="6" icon={<UserOutlined/>} className="enter">
            <Link style={{textDecoration:"none"}} to="/profile">
            Profile
            </Link>
          </Menu.Item>

          </Menu>
        </Header>
        <Content style={{padding: '0 20px', margin:'16px 0'}}>
        <div className="site-layout-content">
        <Row>
          <Col style={{textAlign:"center"}} span={8}><h3>Daily Report</h3></Col>
          <Col style={{textAlign:"center"}} span={8}><h3>Weekly Report</h3></Col>
          <Col style={{textAlign:"center"}} span={8}><h3>Monthly Report</h3></Col>
        </Row>
        </div>     
        </Content>
     </Layout>
                
    </div>
    )
}

}

export default Report;