import React from 'react';
import {Typography} from 'antd';

const {Text} = Typography;

const Error = ({ touched, message}) =>{
    if(!touched){
        return <Text type="danger"></Text>
    }
    if(message){
        return <Text type="danger">{message}</Text>
    }

    return <Text type="success">All good!</Text>
};


export default Error;