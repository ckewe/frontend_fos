import {AppTypes} from '../actions/cartaction';
import { createReducer } from 'reduxsauce';


const initialState = {
    status: false,
    foodCreated: false,
    message: [],
    foodList: [],
    foodName: "",
    foodPrice: "",
    foodSession: "",
    foodDays: "",
    foodType: "",
    startDate: "",
    dessert: "",
}

export const getMenuSuccess = (state,{menu}) =>({
    ...state,
    foodList: menu,
})

export const foodFindSuccess = (state,{foodInfo}) =>({
    ...state,
    foodName: foodInfo.name,
    foodPrice: foodInfo.price,
    foodSession: foodInfo.session,
    foodDays: foodInfo.days,
    foodType: foodInfo.type,
    startDate: foodInfo.start_from,
    dessert: foodInfo.dessert,
})


export const reducer = createReducer(initialState,{
    [AppTypes.GET_MENU_SUCCESS]: getMenuSuccess,
    [AppTypes.FOOD_FIND_SUCCESS]: foodFindSuccess,
});

