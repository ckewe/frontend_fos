import { createReducer } from 'reduxsauce';
import {AppTypes} from '../actions/useraction';
const initialState ={
    email: '',
    name: '',
    studentid: '',
    class: '',
    isLogged: false,
    token: '',
    registerStatus: false,
    userList: [],
}

export const appAuthenticateSuccess = (state,{user}) =>({
        ...state,
        isLogged: user.success,
        token: user.token,
        email: user.email,
        name: user.name,
        class: user.class,
        studentid: user.studentid,
})
    
export const appRegistrationSuccess = (state)=>({
        ...state,
        registerStatus: true,
    
})

export const appLogoutSuccess = (state, {bool})=>({
    ...state,
    isLogged: bool,
    registerStatus: bool,
    email: '',
    token: '',
})

export const getUserListSuccess = (state,{userlist}) =>({
    ...state,
    userList: userlist,
})

export const reducer = createReducer(initialState,{
    [AppTypes.APP_AUTHENTICATE_SUCCESS]: appAuthenticateSuccess,
    [AppTypes.APP_REGISTRATION_SUCCESS]: appRegistrationSuccess,
    [AppTypes.APP_LOGOUT_SUCCESS]: appLogoutSuccess,
    [AppTypes.GET_USER_LIST_SUCCESS]: getUserListSuccess,
});

