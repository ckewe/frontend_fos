import {AppTypes} from '../actions/orderaction';
import {createReducer} from 'reduxsauce';

const initialState = {
    orderList: [],
}

export const viewOrderSuccess = (state,{orderlist})=>({
    ...state,
    orderList: orderlist,
});

export const reducer = createReducer(initialState,{
    [AppTypes.VIEW_ORDER_SUCCESS]: viewOrderSuccess,
    
})