import {combineReducers} from 'redux';
//import userReducer from './userReducer';
//import registerReducer from './registrationReducer';
import {reducer as cartReducer} from './cartReducer';
import {reducer as AppReducer} from './userReducer';
import {reducer as OrderReducer} from './orderReducer';

 const rootReducer = combineReducers({
    app: AppReducer,
    cart: cartReducer,
    order: OrderReducer,
});

export default rootReducer


