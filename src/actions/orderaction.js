import {createActions} from 'reduxsauce';

const {Types, Creators} = createActions({
    viewOrder: null,
    placeOrder: ['orderInfo'],
    viewOrderFail: ['errorMessage'],
    viewOrderSuccess:['orderList'],
    placeOrderSuccess:['successMessage'],
    placeOrderFail:['errorMessage'],
})

export const AppTypes = Types;
export default Creators;