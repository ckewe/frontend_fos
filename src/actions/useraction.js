import {createActions} from 'reduxsauce';


const {Types, Creators} = createActions({
    login: ['email','password'],
    registration: ['user'],
    logout: null,
    getUserList: null,
    deleteUser: ['id'],
    appChangePassword: ['old_password','new_password','confirm_password'],
    appRegistrationSuccess:['user','successMessage'],
    appRegistrationFail:['errorMessage'],
    appAuthenticateSuccess: ['user', 'successMessage'],
    appAuthenticateFail: ['errorMessage'],
    appLogoutSuccess: ['bool','successMessage'],
    getUserListSuccess: ['userlist'],
    getUserListFail: ['errorMessage'],
    deleteUserSuccess: ['successMessage'],
    deleteUserFail: ['errorMessage'],
    appChangePasswordSuccess: ['successMessage'],
    appChangePasswordFail: ['errorMessage'],
})

export const AppTypes = Types;
export default Creators;
