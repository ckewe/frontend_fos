import {createActions} from 'reduxsauce';


const {Types, Creators} = createActions({
    updateFood: ['id'],
    deleteFood: ['id'],
    createFood:['foodInfo'],
    findFood: ['id'],
    getMenu: null,
    getMenuSuccess: ['menu'],
    getMenuFail: ['errorMessage'],
    foodAddSuccess: ['successMessage'],
    foodAddFail: ['errorMessage'],
    foodDeleteSuccess: ['successMessage'],
    foodDeleteFail: ['errorMessage'],
    foodUpdateSuccess: ['successMessage'],
    foodUpdateFail: ['errorMessage'],
    foodFindSuccess: ['foodInfo'],
    foodFindFail:null,

})

export const AppTypes = Types;
export default Creators;
