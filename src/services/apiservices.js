import axios from 'axios';

const getApi = (url, token, contentType = 'application/json') =>{
    let flag = false;
    let message = '';
    let data =[];

    if(token === null || token === ''){
        return{
            message: 'Session expired',
            data,
        };
    }

    return axios({
        method: 'get',
        url: `${url}`,
        headers: {
            Accept: 'application/json',
            'content-type': contentType,
            Authorization: `Bearer ${token}`
        },
    })
    .then(data =>{
        console.log(data)
        if(data.data.flag === true){
            flag= data.data.flag;
            data= data.data.data;
          
        }
        else{
            
        }
        return{
            data,
            flag,
            message
        };
        
    })
    .catch(error =>
        console.log(error)      
    );
}

const postApi = (url, token, postData, contentType = 'application/json') =>{
    console.log('apiservice is callsed');
    console.log(postData);
    console.log(url);
    let newToken = 'null';
    let flag = false;
    let message = '';
    let name = '';
    let email = '';
    let studentid = '';
    let sclass = '';
    if (token === null || token === '') {
        return {
          newToken,
          flag,
          message: 'Session expired'
        };
      }

    return axios({
        method: 'post',
        url: url,
        data: postData,
        headers: {
            Accept: 'application/json',
            'content-type': contentType,
            Authorization: `Bearer ${token}`,
        },
    })
    .then(data =>{
        console.log(data);
        if(data.data.flag === true){
            if(data.data.data.token !== 'undefined'){
                newToken = data.data.data.token;
            }
            if(data.data.data.email){  
                name = data.data.data.name;
                email = data.data.data.email;
                studentid = data.data.data.student_id;
                sclass = data.data.data.class;
            }
            message = data.data.message;
            flag = data.data.flag;

        }
        else{
            newToken = '';
            flag = false;
            message = data.data.message;
        }

        return{
            newToken,
            flag,
            message,
            name,
            email,
            studentid,
            sclass,
        };
    })
    .catch(function catchError(error){
        console.log(error);
        console.log(error.message);
        console.log(error.flag);   
    }
    );

}

const deleteApi = (url, token, contentType='application/json') =>{
    console.log('delete api service');
    let flag = false;
    let message = '';
    if (token === null || token === '') {
        return {
          flag,
          message: 'Session expired'
        };
    }

    return axios({
        method: 'delete',
        url: url,
        headers:{
            Accept: 'application/json',
            'content-type': contentType,
            Authorization: `Bearer ${token}`,
        }
    })
    .then(data =>{
        console.log(data);
        if(data.data.flag){
            message = data.data.message;
            flag = data.data.flag;
        }
        else{
            message = data.data.message;
        }

        return{
            flag,
            message
        };
    })
    .catch(error =>
        console.log(error)      
    );


}

const putApi = (url, token, data,contentType='application/json')=>{
    console.log('put service is called');
    let flag = false;
    let message = '';
    if (token === null || token === '') {
        return {
          flag,
          message: 'Session expired'
        };
    }

    return axios({
        method: 'put',
        url: url,
        data: data,
        headers: {
            Accept: 'application/json',
            'content-type': contentType,
            Authorization: `Bearer ${token}`,
        },
    })
    .then(data =>{
        console.log(data);
        if(data.data.flag === true){
            message = data.data.message;
            flag = data.data.flag;
        }
        else{
            if(data.data.message.new_password || data.data.message.new_password)
            message = data.data.message;
        }

        return{
            flag,
            message
        };
    })
    .catch(error =>
        console.log(error)      
    );
}

export default {
    getApi,
    postApi,
    deleteApi,
    putApi,
}