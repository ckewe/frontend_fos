import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import Login from './components/login';
import Checkout from './components/checkout';
import StudentList from './components/StudentList';
import Report from './components/Report';
import Profile from './components/profile';
import {Route, BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
import * as serviceWorker from './serviceWorker';


function Home(){
    return (
      <Provider store={store}>
      <BrowserRouter>
        <Route path="/" exact component={Login}/>
        <Route path="/home" component={App}/>
        <Route path="/checkout" component={Checkout}/>
        <Route path="/studentlist" component={StudentList}/>
        <Route path="/report" component={Report} />
        <Route path="/profile" component={Profile}/>
      </BrowserRouter>
      </Provider>    
      )
}

ReactDOM.render(<Home/>,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
